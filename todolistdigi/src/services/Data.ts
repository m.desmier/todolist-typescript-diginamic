import TaskInterface from "../Interface/TaskInterface";

export default class Data {
    static url: string = "http://localhost:3001/tasks";

    static loadTasks(): Promise<TaskInterface[]> {
        return fetch(this.url)
        .then(response => {
            return response.json();
        })
        .then(tasks => {
            return tasks;
        })
        .catch(error => {
            console.error("Error attrapée dans le loadTasks", error)
        })
    }
    static statusIsDone(id: number, done: boolean){
        return fetch(this.url+ "/" +id,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "PATCH",
            body: JSON.stringify({ done })
        })
        .then((res) => { return res.json() })
        .catch((error) => { console.log(error) })
    }
    static deleteTask(id: number){
        return fetch(this.url + "/" + id,
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "DELETE",
        })
        .then((res) => { return res.json() })
        .catch((error) => { console.log(error) })
    }
    static addTask(task: TaskInterface): Promise<TaskInterface>{
        return fetch(this.url,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: "POST",
            body: JSON.stringify( task ),
        })
        .then((res) => { return res.json() })
        .catch((error) => { console.log(error) })
    }
}