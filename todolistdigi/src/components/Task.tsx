import TaskInterface from '../Interface/TaskInterface';
import MoveButton from './MoveButton';

// interface TaskInterfaceProps extends TaskInterface {
//     handleClickValidate: () => void; // et pas ()=> {}
// }

const Task = (props: TaskInterface & { onClickValidate: Function; onDeleteTask: Function }) => {
    const isDone = props.done ? "item-done" : "";
    return (
        <section className='d-flex justify-content-between my-4'>
            {/* ne pas oublier d'ajouter onMoveTask à l'interface  */}
            <MoveButton onMoveUp={() => props.onMoveTask(props.id, 'up')} onMoveDown={() => props.onMoveTask(props.id, 'down')}/>
            <h2 className={isDone}>{props.description}</h2>
            <div>
                <button onClick={ (event) =>{
                    props.onClickValidate (event, props.id);
                } } 
                className='btn btn-success me-3'>{props.done ? "Invalider" : "Valider"}</button>
                <button onClick={ (event)=> {
                    props.onDeleteTask(props.id)
                } } className='btn btn-danger me-3'>Supprimer</button>
            </div>
        </section>
    );    
}

export default Task;