// import logo from './logo.svg';
import './App.css';
import Task from './Task';
import { useState, useEffect } from 'react';
import TaskInterface from '../Interface/TaskInterface';
import Data from '../services/Data';

function App() {
  const [tasks, setTasks] = useState<TaskInterface[]>([]);
  useEffect(() => {
    (async() => {
        const loadedTasks: TaskInterface[] = await Data.loadTasks();
        // Modification du state
        setTasks(loadedTasks);
    })();
  }, [])
  const handleClickValidate = async (event: Event, task_id: number) => {
    console.log(`Dans handleClickValidate`, task_id);
    Data.statusIsDone(task_id, true);
    // MAj
    const updateTasks = tasks.map(task => {
      if(task.id === task_id){
        task.done = !task.done;
        Data.statusIsDone(task_id, task.done);
      }
      return task;
    });
      setTasks(updateTasks);
  }
  const sorted_task = [...tasks].sort((a, b) => {
    if(a.done === b.done){
      return -1;
    }else{
      return 1;
    }
  }); 
  const deleteTask = async (id: number) =>{
    await Data.deleteTask(id);
    const updateTasks = tasks.filter(task => task.id !== id);
    setTasks(updateTasks)
  }
  const [newTaskDescription, setNewTaskDescription] = useState('');
  const handleAddTask = async () => {
    if(newTaskDescription.trim() === ""){
      return; // ne fera rien si la description est vide
    }
    const newTask: TaskInterface & { onMoveTask: (direction: 'up' | 'down') => void } = {
      id: tasks.length +1, // génère id
      description: newTaskDescription,
      done: false, // toujours à false 
    }
    await Data.addTask(newTask);
    setTasks([...tasks, newTask]);
    setNewTaskDescription('');
  };
  // const handleMoveTask = (id: number, direction: 'up' | 'down') => {
  //   const index = tasks.findIndex(task => tasks.id === id);
  //   if(index === -1){return;} // tâche pas trouvé
  //   const newTask = [...tasks];
  //   if(direction === 'up' )
  // };
  return (
    <div className="App container">
      <h1>Listes des tâches</h1>
      <div>
        <input type="text" value={newTaskDescription} className="form-control form-control-lg mx-0" placeholder="Ajouter nouvelle tâche..." onChange={(event) => setNewTaskDescription(event.target.value)} />
        <button type="submit" className="btn btn-info mt-3" onClick={handleAddTask}>Ajouter</button>
      </div>

      { sorted_task.map((task) => <Task key={task.id} {...task} onClickValidate={ handleClickValidate } onDeleteTask={ deleteTask } /*onMoveTask={handleMoveTask} */ />) }
    </div>
  );
}

export default App;
