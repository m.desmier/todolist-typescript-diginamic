import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons';

interface MoveButtonProps {
    onMoveUp: () => void;
    onMoveDown: () => void;
}

const MoveButton:  React.FC<MoveButtonProps> = ({ onMoveUp, onMoveDown }) => {
    return (
        <div>
            <button onClick={onMoveUp} className='btn btn-secondary me-2'>
                <FontAwesomeIcon icon={faArrowUp} />
            </button>
            <button onClick={onMoveDown} className='btn btn-secondary'>
                <FontAwesomeIcon icon={faArrowDown} />
            </button>
        </div>
    );
}

export default MoveButton;